package com.zhexiao.convert.exceptions;

/**
 * @author zhe.xiao
 * @date 2021-02-01 10:23
 */
public class WordException extends RuntimeException {
    public WordException(String message) {
        super(message);
    }

    public WordException(Throwable cause) {
        super(cause);
    }
}
